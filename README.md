# Sunflower Project

The purpose of this project is to assist me in copying and converting large numbers of files. These are typically held on users' drives and, when I copy them to my computer they often need to undergo processes in addition to pure copying:

Shortening paths: some files are in very heavily nested folders, sometimes with long names and copying can be hindered by this). 
Cleaning up folder/file names: For a variety of reasons, there are often characters in the fully qualified paths which can cause problems (e.g. "+-?*/").
Updating file formats: e.g. from .DOC to .DOCX
Converting file formats: most often from old "retired" formats (e.g. AMI Pro, AppleWorks) to new ones, or at other times just to simplify things (e.g. TIFF->JPG).

The Sunflower package uses a number of folders and paths, as listed below:
SF\  The Sunflower program directory, containing the software.
Source\ This folder contains the files to be copied/converted
Destination\ This folder will contain the output of Sunflower's efforts.
Job folder\ This folder contains the SF_config.txt, Job_config.txt and log.txt files. The Job folder can be placed anywhere, although it is likely it will be either in the destination folder or next to it.

Overview:  
The stages of running SF are as follows

0:	SF prompts user for source and destination folders and for the location of the job folder.
1:	Make source file list, store in job\
2:	Study files, produce report and save in job\
3:	User makes choices which go in config file (saved in job_config.txt)
4:	SF goes through file list, applies choices specified in job_config.txt. and takes action, either internally or by caling other software via the shell as configured in the <meta-config> file.
5:	Deduplicate (optional, perhaps separate project)


## IN DETAIL

1: A source file list can be created using DIR /S /B <source folder> and the result can be redirected to a file, e.g:   DIR /S /B F:\Users\OronJ >D:\file_list.txt
Alternatively, SF can read the all the files in the destination folder one at a time.

NB: empty folders should be ignored.

2:  Produce a list of file types (by extension) and the file count for each (e.g. "JPG	33 files")
	Report total number of folders, files and bytes
	Report date of earliest and latest files in the list (preferably by looking at modification dates)
	Report list of very short files (less than x bytes, probably 1 or 2 but should be configurable)
	Report list of files whose complete path exceeds a configurable threshold (240 by default?_

3:	Job_config.txt options:
Ignore List (list of files and folder. Patterns may be used)
Copy List (list of files and folder. Patterns may be used, overrides ignore list)
Actions:  (delete/ignore, COPY, CONVERT \<conversion>, multiple conversions allowed)
Clean Names:  removes certain chars (e.g. trailing spaces) and substitutes others as per meta-config list.

4:	\<meta-config> Description:
A file consisting of rules for types of conversions and for cleaning up file paths. It is essentially a list of default actions which are specifically selected in job_config.txt.

There are a few variables which are used in \<meta-config>:
[source]	Path to source folder
[destination]	Path to destination folder
[folder]	Path to current file excluding file name)
[fqfn]		Source file name including full path and leaf
[fn]		File name including extension (i.e. [base].[ext])
[base]		File name without extension
[ext]		File name extension

CONVERSION lines are given a unique label, followed by a space and the shell command, typically including variables to be substituted with the actual file names or paths. For example:

ODT C:\Program Files\LibreOffice\soffice.exe --convert-to odt [source] [destination]

JPG C:\Program Files (x86)\IrfanView\i_view32.exe [source] /convert=[destination][fn].jpg

JPG2PNGx500pt C:\Program Files (x86)\i_view32.exe [source] /resize=(500,300) /aspectratio /resample /convert=[destination][fn].png

JPG2PDF JPG C:\Program Files (x86)\IrfanView\i_view32.exe [source] /convert=[destination][fn].PDF

Cleaning options:
TRIM, LTRIM, eliminate character, substitute character X for Y
Also, replace character runs (that is, 2 or more consequtive full stops) with a single character. The runs I can think of at the moment are full stops and spaces. There may be others.

Dealing with filename duplicates:
If, as a result of the cleaning operations above, a file ends up having the same name as one already at the destination, the programme should alert the user to the fact and offer them the following optiosn:
1. Rename this file manually
2. Add a suffix and an incremented number(user to provide suffix). E.g. #22. Note that this is added to [base], not to [ext]!


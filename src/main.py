import argparse
import sys
import os
import json

ARG_INIT = "init"
ARG_REPORT = "report"
ARG_CONVERT = "convert"


def main():
    args = get_args()
    print(args)

    if args.command == ARG_INIT:
        init(args.job, args.src, args.dst)
    elif args.command == ARG_REPORT:
        print("report")
    elif args.command == ARG_CONVERT:
        print("convert")
    else:
        assert False, "`args.command` should be checked before here."


# Reads arguments from command line and returns a Namespace object.
def get_args():
    parser = argparse.ArgumentParser(description="Sunflower")
    subparsers = parser.add_subparsers(dest="command")

    init_parser = subparsers.add_parser(ARG_INIT)
    init_parser.add_argument("job")
    init_parser.add_argument("src")
    init_parser.add_argument("dst")

    report_parser = subparsers.add_parser(ARG_REPORT)
    report_parser.add_argument("job")

    convert_parser = subparsers.add_parser(ARG_CONVERT)
    convert_parser.add_argument("job")

    ret = parser.parse_args()
    if ret.command is None:
        parser.parse_args(["-h"])

    return ret


# Takes the names of three directories and initialises
# the job directory for work with sunflower.
# If the job dir an dst dir don't already exist then
# they will be created, but src_dir should already exist.
def init(job_dir, src_dir, dst_dir):
    # TODO: exception handling
    # TODO: should we check that src_dir exists?
    os.makedirs(job_dir, exist_ok=True)
    os.makedirs(dst_dir, exist_ok=True)

    config_path = os.path.join(job_dir, ".sunflower")
    config_content = {
        "source": src_dir,
        "destination": dst_dir,
    }

    with open(config_path, "w") as config_file:
        config_file.write(json.dumps(config_content, indent=4))
        config_file.write("\n")
        print(f"wrote config file {config_path}")


if __name__ == "__main__":
    main()
